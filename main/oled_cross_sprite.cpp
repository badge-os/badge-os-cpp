#include "app.h"

#define TAG "OLED cross"

#include "nano_engine.h"

#ifdef OLED_USE_LOW_MEM
typedef NanoEngine1 NanoEngineSSD1306;
#else
typedef NanoEngine<TILE_128x64_MONO> NanoEngineSSD1306; //<= memory corruption! - need more stack!!!!
#endif
NanoEngineSSD1306 engine;
NanoFixedSprite<NanoEngineSSD1306, engine> sprite( {0,0}, {8,8}, crossImage);
//NanoSprite<NanoEngineSSD1306, engine> sprite( {0,0}, {8,8}, crossImage);

bool drawAll()
{
    engine.canvas.clear();
    engine.canvas.drawBitmap1(32, 0, 64, 64, nnclogo);
    sprite.draw();
    return true;
}

void setup_cross_sprite()
{
    engine.setFrameRate( 10 );
    engine.begin();
    engine.drawCallback( drawAll );
    engine.canvas.setMode(CANVAS_MODE_TRANSPARENT);
    engine.refresh();
}

void loop_cross_sprite()
{
    if (!engine.nextFrame()) return;
    // You will see horizontal flying cross
    
    if( sprite.y()>= oled_height() )
      sprite.moveTo( { random(oled_width()), 0 } );
    else
      sprite.moveBy( { 0, 1 } );

    engine.display();                // refresh display content
}


void oled_cross_sprite_task(void *pvParameters) 
{
  ESP_LOGD(__FUNCTION__, "Starting...");
  setup_cross_sprite();
  while (1) {
    loop_cross_sprite();
    vTaskDelay(5);
  }
  vTaskDelete(NULL); 
}

