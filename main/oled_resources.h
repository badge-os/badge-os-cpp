#pragma once

extern const uint8_t nnclogo[];
extern const uint8_t owl[];
extern const uint8_t tm_logo[];
extern const uint8_t vvs_logo[];
extern const PROGMEM uint8_t heartImage[8];
extern const PROGMEM uint8_t crossImage[8];
extern const PROGMEM uint8_t AsteroidsImage[8][8];
extern const PROGMEM uint8_t shipImage[8];
extern const uint8_t rocket1[];
extern const uint8_t rocket2[];
extern const uint8_t rocket3[];
extern const uint8_t rocket4[];
extern const uint8_t rocket5[];
extern const uint8_t rocket6[];

extern const uint8_t fighter_stop_corona[];
extern const uint8_t fighter_masks[];
extern const uint8_t fighter_stay_at_home[];
extern const uint8_t fighter_or[];
extern const uint8_t fighter_covid_die[];
extern const uint8_t fighter_covid_cancel[];

extern const PROGMEM uint8_t CoronaImage[4][8];
extern const PROGMEM uint8_t fighterImage[8];
extern const PROGMEM uint8_t bulletImage[8];


