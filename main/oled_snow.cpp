#include "app.h"

#define TAG "OLED Snow"

#include "nano_engine.h"

//https://github.com/lexus2k/ssd1306/blob/master/examples/nano_engine/snowflakes/snowflakes.ino

/*
 * Define snowflake images directly in flash memory.
 * This reduces SRAM consumption.
 * The image is defined from bottom to top (bits), from left to right (bytes).
 */
const PROGMEM uint8_t snowFlakeImage[8][8] =
{
    {
        0B00111000,
        0B01010100,
        0B10010010,
        0B11111110,
        0B10010010,
        0B01010100,
        0B00111000,
        0B00000000
    },
    {
        0B00010000,
        0B01010100,
        0B00111000,
        0B11101110,
        0B00111000,
        0B01010100,
        0B00010000,
        0B00000000
    },
    {
        0B00111000,
        0B00010000,
        0B10111010,
        0B11101110,
        0B10111010,
        0B00010000,
        0B00111000,
        0B00000000
    },
    {
        0B00011000,
        0B01011010,
        0B00100100,
        0B11011011,
        0B11011011,
        0B00100100,
        0B01011010,
        0B00011000
    },
    {
        0B00010000,
        0B00111000,
        0B01010100,
        0B11101110,
        0B01010100,
        0B00111000,
        0B00010000,
        0B00000000
    },
    {
        0B10000010,
        0B00101000,
        0B01101100,
        0B00010000,
        0B01101100,
        0B00101000,
        0B10000010,
        0B00000000
    },
    {
        0B01000100,
        0B10101010,
        0B01101100,
        0B00010000,
        0B01101100,
        0B10101010,
        0B01000100,
        0B00000000
    },
    {
        0B00101000,
        0B01010100,
        0B10111010,
        0B01101100,
        0B10111010,
        0B01010100,
        0B00101000,
        0B00000000
    },
};

NanoEngine1 engine;
typedef NanoFixedSprite<NanoEngine1, engine> CSnowFlake;

class SnowFlake: public CSnowFlake
{
public:
    SnowFlake()
    : CSnowFlake({0, 0}, {8, 8}, nullptr) 
    { 
    }

    bool isAlive() { return falling; }

    void bringToLife()
    {
        int snow_idx = random(8);
        setBitmap( snowFlakeImage[snow_idx] );
        /* Set initial position in scaled coordinates */
        scaled_position = { random(ssd1306_displayWidth() * 8), -8 * 8 };
        /* Use some random speed */
        speed = { random(-16, 16), random(4, 12) };
        /* After countdown timer ticks to 0, change X direction */
        timer = random(24, 48);
        moveTo( scaled_position/8 );
        falling = true;
    }

    void move()
    {
        scaled_position += speed;
        timer--;
        if (0 == timer)
        {
            /* Change movement direction */
            speed.x = random(-16, 16);
            timer = random(24, 48);
        }
        moveTo( scaled_position/8 );
        if (y() >= static_cast<lcdint_t>(ssd1306_displayHeight()) )
        {
            falling = false;
        }
    }

private:
    NanoPoint scaled_position;
    NanoPoint speed;
    uint8_t timer;
    bool falling = false;
};

static const uint8_t maxCount = 20;

// These are our snow flakes
SnowFlake snowFlakes[maxCount];

bool onDraw()
{
    engine.canvas.clear();
    engine.canvas.drawBitmap1(32, 0, 64, 64, nnclogo);
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (snowFlakes[i].isAlive())
        {
            snowFlakes[i].draw();
        }
    }
    return true;
}

void setup_snow()
{
//    ssd1306_128x64_i2c_init();
//    ssd1331_96x64_spi_init(3,4,5);
//    ssd1351_128x128_spi_init(3,4,5);
//    il9163_128x128_spi_init(3,4,5);
    engine.setFrameRate( 10 );
    engine.begin();
    engine.drawCallback( onDraw );

    engine.canvas.setMode(CANVAS_MODE_TRANSPARENT);
    engine.refresh();
}

void addSnowFlake()
{
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (!snowFlakes[i].isAlive())
        {
            snowFlakes[i].bringToLife();
            break;
        }
    }
}


void moveSnowFlakes()
{
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (snowFlakes[i].isAlive())
        {
            snowFlakes[i].move();
        }
    }
}


static uint8_t globalTimer=3;

void loop_snow()
{
    if (!engine.nextFrame()) {
      return;
    }
    if (0 == (--globalTimer))
    {
        // Try to add new snowflake every ~ 90ms 
        globalTimer = 3;
        addSnowFlake();
    }
    moveSnowFlakes();
    engine.display();
}

void oled_snow_task(void *pvParameters) 
{
  ESP_LOGD(__FUNCTION__, "Starting...");
  setup_snow();
  ssd1306_clearScreen();
  ssd1306_drawBuffer(32, 0, 64, 64, nnclogo);

  while (1) {
    loop_snow();
    vTaskDelay(10);
  }
  vTaskDelete(NULL); 
}
