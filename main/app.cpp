#include "app.h"

static const char *TAG = __FILE__;

void self_test(void *pvParameters) {
  LOG_FUNC_BEGIN(TAG)
  const int delay1 = 10;
  const int delay2 = 50; 
  const int LED_OFF = 1;
  const int LED_ON = 0;

  gpio_set_level(PIN_LED_BLUE, LED_OFF);
  while (1) {
    if (touchpad_get_state()==TOUCHPAD_STATE_OFF)
    {
      vTaskDelay(delay2); //must do task delay to not block threads
      continue;
    }
    gpio_set_level(PIN_LED_BLUE, LED_ON);
    vTaskDelay(delay1);
    gpio_set_level(PIN_LED_BLUE, LED_OFF);
    vTaskDelay(delay1);
    gpio_set_level(PIN_LED_BLUE, LED_ON);
    vTaskDelay(delay1);
    gpio_set_level(PIN_LED_BLUE, LED_OFF);
    vTaskDelay(delay2);
  }
  LOG_FUNC_END(TAG)
  vTaskDelete(NULL);
}

void Delay(int ms)
{
    vTaskDelay(pdMS_TO_TICKS(ms));
}

extern "C" {
    void app_main(void);
}

void app_main() 
{
  /* Disable buffering on stdin */
  setvbuf(stdin, NULL, _IONBF, 0);

  nvs_initialize();
  vfs_initialize("storage",VFS_MOUNT_PATH);
  console_init();

//  esp_log_level_set("*", ESP_LOG_NONE);
//  esp_log_level_set("*", ESP_LOG_DEBUG);
  esp_log_level_set("*", ESP_LOG_WARN);
  

  printf(LOG_COLOR_W
         "\n\n\nNoNameBadge created by TechMaker https://techmaker.ua/\r\n"
         "NNC Badge - Examples by BlackVS aka VVS\r\n"
         );

  
  /* Print chip information */
  esp_chip_info_t chip_info;
  esp_chip_info(&chip_info);
  printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
          chip_info.cores,
          (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
          (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

  printf("silicon revision %d, ", chip_info.revision);

  printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
          (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");



  time_t now;
  char strftime_buf[64];
  struct tm timeinfo;

  time(&now);
  // Set timezone to China Standard Time
  setenv("TZ", "EET-2", 1);
  tzset();

  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
  printf("The current date/time is: %s\n", strftime_buf);

  //init board GPIO
  gpio_init(); // all gpio init staff here - to avoid conflicts between models like MPU, OLED etc

  //tasks init - variables etc

  leds_task_init();
  touchpad_task_init(); //JTAG conflict!!!
  mpu_task_init();  //i2c init moved to gpio_init
  oled_task_init(); //i2c init moved to gpio_init
  
  WiFi.init();

  leds_alarm_set(false);

  //Touchpad: reads input
  xTaskCreate(&touchpad_task, "Touchpad", 1024 * 4, NULL, 5, NULL);

  //MPU: reads sensora
	xTaskCreate(&mpu_task , "MPU" , 4096, NULL, 5, &mpu_task_handle);

  //Main blue LED" hearbeat on touchpad pressed
  xTaskCreate(&self_test, "SelfTest", 1024 * 2, NULL, 5, NULL);
  
  #ifdef BADGE_LEDS1
  xTaskCreate(&leds_task, "LEDs n ROLL", 1024 * 4, NULL, 5, NULL);
  xTaskCreate(&oled_heart_sprite_oop_task, "OLED Heart sprite OOP", 4*1024, NULL, 5, NULL);
  #endif

  //Compass+Snow
  #ifdef BADGE_LEDS2
  xTaskCreate(&leds_compass_task, "LEDs compass", 1024 * 4, NULL, 5, NULL);
  xTaskCreate(&oled_snow_task, "OLED Snow", 4*1024, NULL, 5, NULL);
  #endif
  
  //Space ship - Asteroids
  #ifdef BADGE_ASTEROIDS
  xTaskCreate(&leds_task, "LEDs n ROLL", 1024 * 4, NULL, 5, NULL);
  xTaskCreate(&oled_ship_task, "OLED Ship", 4*1024, NULL, 5, NULL);
  #endif 

  #ifdef BADGE_CORONA
  //Corona game
  xTaskCreate(&leds_alarm_task, "LEDS Alarm", 1024 * 4, NULL, 5, NULL);
  xTaskCreate(&oled_corona_fighter_task, "CORONA Fighter", 4*1024, NULL, 5, NULL);
  #endif

  //OLED, choose one of:
  //xTaskCreate(&oled_logo_task, "OLED", 1024 * 4, NULL, 5, NULL);
  //xTaskCreate(&oled_cross_sprite_task, "OLED Cross sprite", OLED_TASK_STACK, NULL, 5, NULL);
  //xTaskCreate(&oled_space_ship_task, "OLED Space Ship", 4*1024, NULL, 5, NULL);

  //xTaskCreatePinnedToCore(&console_task, "BadgeOS Shell", 1024 * 6, NULL, 5, NULL, 1);
  console_task(NULL);
}
