#include "app.h"


#define TAG "OLED Ship"

#include "nano_engine.h"

//https://github.com/lexus2k/ssd1306/tree/master/examples/direct_draw/move_sprite

/*
 * Define sprite width. The width can be of any size.
 * But sprite height is always assumed to be 8 pixels
 * (number of bits in single byte).
 */


NanoEngine1 engine;
typedef NanoFixedSprite<NanoEngine1, engine> CSprite;

class CAsteroid: public CSprite
{
public:
    CAsteroid()
    : CSprite({0, 0}, {8, 8}, nullptr) 
    { 
    }

    bool isAlive() { return falling; }

    void bringToLife()
    {
        int snow_idx = random(8);
        setBitmap( AsteroidsImage[snow_idx] );
        /* Set initial position in scaled coordinates */
        scaled_position = { random(ssd1306_displayWidth() * 8), -8 * 8 };
        /* Use some random speed */
        speed = { random(-16, 16), random(4, 12) };
        /* After countdown timer ticks to 0, change X direction */
        timer = random(24, 48);
        moveTo( scaled_position/8 );
        falling = true;
    }

    void move()
    {
        scaled_position += speed;
       
        timer--;
        if (0 == timer)
        {
            /* Change movement direction */
            speed.x = random(-16, 16);
           
            timer = random(24, 48);
        }
        moveTo( scaled_position/8 );
        if (y() >= static_cast<lcdint_t>(ssd1306_displayHeight()) )
        {
            falling = false;
        }
        // printf("Asteroid X: %i, Y: %i \n", speed.x, speed.y);
    }

private:
    NanoPoint scaled_position;
    NanoPoint speed;
    uint8_t timer;
    bool falling = false;
};


class CShip: public CSprite
{
public:
    CShip()
    : CSprite({0, 0}, {8, 8}, shipImage) 
    { 
    }

    bool isAlive() { return alive; }

    void respawn()
    {
        position = { random(ssd1306_displayWidth()), -8 };
        moveTo( position );
        alive = true;
    }

    void move()
    {
        CMPUData data;
        MPU_get(data);
        //range: (roll[-180,180]  pitch[-90,90]  yaw[-180,180])
        float scaleX=0.4f;
        float scaleY=0.8f;
        int dx=int(data.pitch*scaleX);
        int dy=int(data.roll*scaleY);
        //limit speed
        dx= MIN(5,MAX(-5,dx)); 
        dy= MIN(5,MAX(-5,dy));
        //move
        int nx = position.x - dx;
        int ny = position.y + dy;

        position.x=MIN(oled_width() -8, MAX(0,nx));
        position.y=MIN(oled_height()-8 ,MAX(0,ny));

        moveTo( position );
    }

private:
    NanoPoint position;
    bool alive = false;
};

static const uint8_t maxCount = 2;

// These are our snow flakes
CAsteroid asteroids[maxCount];
CShip     ship;

bool onDraw()
{
    engine.canvas.clear();
    
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (asteroids[i].isAlive())
        {
            asteroids[i].draw();
        }
    }

    if(ship.isAlive())
        ship.draw();

    return true;
}

void setup_game()
{
    engine.setFrameRate( 10 );
    engine.begin();
    engine.drawCallback( onDraw );

    engine.canvas.setMode(CANVAS_MODE_TRANSPARENT);
    engine.refresh();
    //
    ship.respawn();
}

void addAsteroids()
{
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (!asteroids[i].isAlive())
        {
            asteroids[i].bringToLife();
            break;
        }
    }
}


void moveAsteroids()
{
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (asteroids[i].isAlive())
        {
            asteroids[i].move();
        }
    }
}


static uint8_t globalTimer=3;

void moveShip()
{
    if(ship.isAlive())
        ship.move();
}

void loop_game()
{
    if (!engine.nextFrame()) {
      return;
    }
    if (0 == (--globalTimer))
    {
        // Try to add new Asteroids every ~ 90ms 
        globalTimer = 3;
        addAsteroids();
    }
    moveAsteroids();
    moveShip();
    engine.display();
}

void setup_screen_saver(){
  ssd1306_clearScreen();
  ssd1306_drawBuffer(32, 0, 64, 64, rocket1);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket2);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket3);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket4);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket5);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket6);
}

void oled_ship_task(void *pvParameters) 
{
  ESP_LOGD(__FUNCTION__, "Starting...");
  setup_screen_saver();  
  vTaskDelay(250);
  setup_game();

  while (1) {
    loop_game();
    vTaskDelay(5);
  }
  vTaskDelete(NULL); 
}

