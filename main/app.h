#pragma once

#define CONFIG_STORE_HISTORY 1

//#define MAX(x, y) (((x) > (y)) ? (x) : (y))
//#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#include <stdio.h>
//#include <string.h>
#include <string>
#include <vector>

#include "log.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_tls.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_ota_ops.h"

///// netif not supports C++ compiler yet
#ifdef __cplusplus
extern "C" {
#endif

#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_sntp.h"

//#include "lwip/apps/sntp.h"
//#include "lwip/err.h"

#ifdef __cplusplus
}
#endif

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"

#include "driver/gpio.h"
#include "sdkconfig.h"

#include "badge_config.h"

#include "nvs.h"
#include "vfs.h"
#include "console.h"
#include "wifi.h"

//tasks
#include "btn.h"
#include "leds.h"
#include "leds_compass.h"
#include "mpu_task.h"
#include "oled.h"
#include "oled_resources.h"

void Delay(int ms);
