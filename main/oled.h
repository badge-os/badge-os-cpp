#pragma once

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "ssd1306.h"
#include "nano_gfx.h"
#include "nano_engine.h"

//if defined - use TILE_16x16_MONO screen buffer
//if not - use full TILE_128x64_MONO - needs more stack
//#define OLED_USE_LOW_MEM
#ifdef OLED_USE_LOW_MEM
#define OLED_TASK_STACK 4096
#else
#define OLED_TASK_STACK 16384
#endif


extern const uint8_t nnclogo[];
extern const uint8_t tm_logo[];

int oled_width(void);
int oled_height(void);


// IO init - should be called only once
void oled_task_init(void);

// unbuffered output
void oled_draw_logo(void);

// buffered output
extern NanoEngine<TILE_128x64_MONO> oled_engine;
void oled_engine_init(void);
void oled_clear(bool fForceRefresh=false);
void oled_print(uint8_t x, uint8_t y, const char *text, EFontStyle style=STYLE_NORMAL, bool fForceRefresh=false);
void oled_print(uint8_t x, uint8_t y, int value, EFontStyle style=STYLE_NORMAL, bool fForceRefresh=false);
void oled_printf(uint8_t x, uint8_t y, EFontStyle style, const char* format, ... );
void oled_printf_refresh(uint8_t x, uint8_t y, EFontStyle style, const char* format, ... );
    
void oled_drawHLine(lcdint_t x1, lcdint_t y1, lcdint_t x2);
void oled_drawVLine(lcdint_t x1, lcdint_t y1, lcdint_t y2);

void oled_refresh(void);//to show buffered
//setTextSize


//to use with xTaskCreate
void oled_logo_task(void *pvParameters);
void oled_snow_task(void *pvParameters);
void oled_heart_task(void *pvParameters);
void oled_cross_sprite_task(void *pvParameters);
void oled_heart_sprite_oop_task(void *pvParameters);
void oled_space_ship_task(void *pvParameters);
void oled_ship_task(void *pvParameters);
void oled_corona_fighter_task(void *pvParameters);
