# ESP32 driver for BMP280 chip with ESP-IDF

## Example usage

```C
#include <stdio.h>
#include "esp_log.h"
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include <math.h>
#include "bmp280.h"

#define BLINK_GPIO GPIO_NUM_2

#define BMP280_I2C_PORT I2C_NUM_0		// I2C port number
#define BMP280_I2C_SCL_IO PIN_IIC_SCL	// I2C clock GPIO number
#define BMP280_I2C_SDA_IO PIN_IIC_SDA	// I2C data GPIO number
#define BMP280_I2C_FREQ_HZ 100000		// I2C clock frequency

/* Current pressure at sealevel in hPa taken from http://www.aviador.es/Weather/Meteogram/UKKK */
#define BMP280_QNH 1012

static TaskHandle_t bmp280_task_handle = NULL;

void i2c_master_init(i2c_port_t port, gpio_num_t scl_io_num, gpio_num_t sda_io_num, uint32_t clk_speed) {
	i2c_config_t conf = {
		.mode = I2C_MODE_MASTER,
		.scl_io_num = scl_io_num,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.sda_io_num = sda_io_num,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = clk_speed,
	};
	i2c_param_config(port, &conf);
	i2c_driver_install(port, conf.mode, 0, 0, 0);
}

static void bmp280_task(void *arg) {
	static const char *TAG = "bmp280";

	/* Variables for all measurements */
	double temp, press, alt;
	int8_t com_rslt;

	/* Start BMP280 and change settings */
	BMP280_I2C_Set(BMP280_I2C_PORT);

	bmp280_t bmp280;
	bmp280.dev_addr = BMP280_I2C_ADDRESS1;
	com_rslt = BMP280_init(&bmp280);
	com_rslt += BMP280_set_power_mode(BMP280_NORMAL_MODE);
	com_rslt += BMP280_set_work_mode(BMP280_STANDARD_RESOLUTION_MODE);
	com_rslt += BMP280_set_standby_durn(BMP280_STANDBY_TIME_1_MS);
	if (com_rslt != SUCCESS) {
		ESP_LOGE(TAG, "Init failed! Check connection. Terminating task...");
		vTaskDelete(NULL);
	}
	ESP_LOGI(TAG, "Init successful!");

	while (1) {
		/* Read temperature and pressure */
		BMP280_read_temperature_double(&temp);
		BMP280_read_pressure_double(&press);
		/* Calculate current altitude, based on current QNH pressure */
		alt = BMP280_calculate_altitude(BMP280_QNH * 100);
		ESP_LOGI(TAG, "Temp : %6.2f C   Press: %6.0f Pa    Alt  : %3.0f m", temp, press,
				 alt);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

void app_main() {
	/* Init LED */
	gpio_pad_select_gpio(BLINK_GPIO);
	/* Set the GPIO as a push/pull output */
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_INPUT_OUTPUT);

	i2c_master_init(BMP280_I2C_PORT, BMP280_I2C_SCL_IO, BMP280_I2C_SDA_IO, BMP280_I2C_FREQ_HZ);

	xTaskCreate(bmp280_task, "bmp280 task", 4096, NULL, 5, &bmp280_task_handle);
}

```